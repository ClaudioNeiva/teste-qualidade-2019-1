package br.ucsal.bes.testequalidade20191.atividade02mvn;

import java.util.Scanner;

public class Calculo {

	public static void main(String[] args) {
		Integer n;
		Long fatorial;

		n = obterNumero();
		fatorial = calcularFatorial(n);
		exibirFatorial(n, fatorial);
	}

	public static Integer obterNumero() {
		Scanner scanner = new Scanner(System.in);
		Integer n;
		while (true) {
			System.out.println("Informe um n�mero (0 a 100, intervalo fechado):");
			n = scanner.nextInt();
			if (n >= 0 && n <= 100) {
				return n;
			} else {
				System.out.println("N�mero fora da faixa!");
			}
		}
	}

	public static Long calcularFatorial(Integer n) {
		Long fatorial = 1L;
		for (Integer i = 1; i <= n; i++) {
			fatorial *= i;
		}
		return fatorial;
	}

	public static void exibirFatorial(Integer n, Long fatorial) {
		System.out.println("Fatorial(" + n + ")=" + fatorial);
	}

}
