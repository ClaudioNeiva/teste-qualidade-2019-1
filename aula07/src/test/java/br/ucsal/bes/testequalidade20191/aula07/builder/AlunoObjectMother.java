package br.ucsal.bes.testequalidade20191.aula07.builder;

import br.ucsal.bes.testequalidade20191.aula07.domain.Aluno;
import br.ucsal.bes.testequalidade20191.aula07.domain.SituacaoEnum;

public class AlunoObjectMother {

	public static Aluno umAlunoTrancadoMaiorDeIdade() {
		Aluno aluno = new Aluno();
		aluno.setMatricula(1990);
		aluno.setNome("Antonio Claudio");
		aluno.setSituacao(SituacaoEnum.TRANCADO);
		aluno.setEmail("claudio@ucsal");
		aluno.setAnoNascimento(2000);
		return aluno;
	}

}
