package br.ucsal.bes.testequalidade20191.aula07.builder;

import br.ucsal.bes.testequalidade20191.aula07.domain.Aluno;
import br.ucsal.bes.testequalidade20191.aula07.domain.SituacaoEnum;

public class AlunoBuilder {

	private static final Integer MATRICULO_DEFAULT = 1;
	private static final String NOME_DEFAULT = "Claudio";
	private static final SituacaoEnum SITUACAO_DEFAULT = null;
	private static final String EMAIL_DEFAULT = "claudio@ucsal.br";
	private static final Integer ANO_NASCIMENTO_DEFAULT = null;

	private Integer matricula = MATRICULO_DEFAULT;
	private String nome = NOME_DEFAULT;
	private SituacaoEnum situacao = SITUACAO_DEFAULT;
	private String email = EMAIL_DEFAULT;
	private Integer anoNascimento = ANO_NASCIMENTO_DEFAULT;

	private AlunoBuilder() {
	}

	public static AlunoBuilder umAluno() {
		return new AlunoBuilder();
	}

	public AlunoBuilder comMatricula(Integer matricula) {
		this.matricula = matricula;
		return this;
	}

	public AlunoBuilder comNome(String nome) {
		this.nome = nome;
		return this;
	}

	public AlunoBuilder comEmail(String email) {
		this.email = email;
		return this;
	}

	public AlunoBuilder comAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
		return this;
	}

	public AlunoBuilder comMatriculaTrancada() {
		this.situacao = SituacaoEnum.TRANCADO;
		return this;
	}

	public AlunoBuilder comMatriculaAtiva() {
		this.situacao = SituacaoEnum.ATIVO;
		return this;
	}

	public AlunoBuilder comMatriculaCancelada() {
		this.situacao = SituacaoEnum.CANCELADO;
		return this;
	}
	
	public AlunoBuilder mas(){
		// O que deve ser feito aqui???
		return this;
	}

	public Aluno build() {
		Aluno aluno = new Aluno();
		aluno.setMatricula(matricula);
		aluno.setNome(nome);
		aluno.setEmail(email);
		aluno.setSituacao(situacao);
		aluno.setAnoNascimento(anoNascimento);
		return aluno;
	}

}
