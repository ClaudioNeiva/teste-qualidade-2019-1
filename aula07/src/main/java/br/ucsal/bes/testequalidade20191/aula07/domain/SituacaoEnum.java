package br.ucsal.bes.testequalidade20191.aula07.domain;

public enum SituacaoEnum {

	ATIVO, TRANCADO, CANCELADO;
	
}
