package br.ucsal.bes20191.testequalidade.restaurante.poolista01.implementacaopropria;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.ucsal.bes20191.testequalidade.restaurante.poolista01.CalculosUtil;
import br.ucsal.bes20191.testequalidade.restaurante.poolista01.FatorialUtil;

public class CalculosUtilIntegradoTest {

	private CalculosUtil calculosUtil;

	@Before
	public void setup() {
		FatorialUtil fatorialUtil = new FatorialUtil();
		calculosUtil = new CalculosUtil(fatorialUtil);
	}

	/**
	 * Verificar c�lculo do valor de E. Caso de teste: entrada n=1; sa�da
	 * esperada=2
	 */
	@Test
	public void testarCalculoE1() {
		// Defini��o da entrada
		Integer n = 1;
		// Defini��o da sa�da esperada
		Double eEsperado = 2d;
		// Execu��o do m�todo sobre teste e obten��o do resultado esperado
		Double eAtual = calculosUtil.calcularE(n);
		// Compara��o do resultado esperado com o resultado atual
		Assert.assertEquals(eEsperado, eAtual, 0.0001);
	}

}
