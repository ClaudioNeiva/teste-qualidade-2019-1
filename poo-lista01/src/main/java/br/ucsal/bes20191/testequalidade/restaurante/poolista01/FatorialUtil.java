package br.ucsal.bes20191.testequalidade.restaurante.poolista01;

public class FatorialUtil {

	/**
	 * Cacular o fatorial de n;
	 * 
	 * @param n
	 *            - o valor sobre o qual ser� calculado o fatorial
	 * @return o valor do fatorial
	 */
	public Long calcularFatorial(int n) {
		// Erro de implementa��o para garantir a quebra do teste integrado e
		// demonstrar o isolamento obtido atrav�s do teste unit�rio.
		Long fat = 0L;
		for (int i = 1; i <= n; i++) {
			fat *= i;
		}
		return fat;
	}

}
